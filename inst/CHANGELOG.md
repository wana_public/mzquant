# Changelog

*Version 0.8.3 (01 November 2022)*

-   Add README.md

-   Add build on `bioconductore_docker:latest` and `bioconductor_docker:devel`
    at different levels of production (`dev`:`latest` + `devel`, `main` + 
    `releases`: `latest`)
    
-   Fix an issue in `trimming()`

-   Add man pages for the quantification workflows

-   Rename `devel_1.0` to `dev` branch to reflect `CI/CD` approach


*Version 0.8.2 (28 October 2022)*

-   Fix build issues to be compliant with `devtools::check()`

-   Fix some `CI/CD` issues, runs now also on `devel_1.0` branch


*Version 0.8.1 (27 October 2022)*

-   Update of `vignettes` and `example data`

-   Replacement of the term `peak` by `feature`

-   Customize the selection of input columns of `MZmine3` to ease review

-   Update settings to version `0.8.1` to reflect changes in code

-   Replace `mzquant_id` by `mzquant_id` to prepare for future implementations

-   Implement a common custom compound DB for `MZmine3` and `MZquant`

-   Refactorize functions, variables, and field names to lower case

-   Refactorize whole workflows to less complexity, e.g. less processing steps
    in the quantification workflow

-   Fix localmax position assignment

-   Move model building and model plotting to own functions

-   Reduce default output of csv files

-   Add full custom control on blank correction decisions

-   Refine workflow control in workflow management

-   Add custom controlled output fields in the final result csv

-   Add AGLP-3 license tag to description

-   Make news file in inst

-   Update BioCViews tags

-   Update the vignettes and the demonstration files


*Version 0.8.0 (15 September 2022)*

-   Implement processing of new `MZmine3` `csv` output format

-   Deprecate processing of old `MZmine2` `csv` output format

-   Refactorization to `tidyverse` based functions

-   Update settings to version `0.8.0` to reflect changes in code


*Version 0.7.23*

-   License transferred to `AGPL 3`


*Version 0.7.22 (25 April 2021)*

-   Automated releases implemented in `gitlab-cli`

-   Generate a pre-compiled archive for download


*Version 0.7.21 (24 April 2021)*

-   Replace remote `gitlab` by `devtools` gitlab

-   update `CI` for automated releases


*Version 0.7.20 (24 April 2021)*

-   Replace `csv` examples by internal data files

-   update the code to load internal data files

-   Update the `vignettes`

-   tidy up `CI/CD`


*Version 0.7.18 (22 April 2021)*

-   Replace absolute path in `vignette`

-   Fix corrupt link in help files


*Version 0.7.17 (21 April 2021)*

-   A `vignette` document was integrated in `MZquant`

-   Run `browseVignettes("MZquant")` for view
