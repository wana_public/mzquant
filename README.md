# MZquant

## Introduction

`MZquant` is a comprehensive, semi-automated computational mass
spectrometry workflow for the high-throughput target screening.
`MZquant` builds on the compelling mass spectrometry suite
[`MZmine3`](https://mzmine.github.io "MZmine 3"). `MZmine 3` is required
for preprocessing of the raw mass spectrometrical data. However, it
includes no advanced module for the targeted screening.

`MZquant` processes the `MZmine 3` export files in `csv` format in the
following main steps:

-   Feature list workflow

    -   preprocessing of the `MZmine 3` output file including selection
        of custom-defined columns to remove unused columns from the full
        `MZmine 3` output.

    -   interactive editing of the processed `csv` to justify column
        names or else (optional)

    -   preparation of internal feature lists (e.g., samples, blanks,
        quantification, quality control)

    -   assignment of a standardized name for all compounds to prevent
        name confusions during processing

    -   interactive removal of duplicate annotations and review of
        missing compounds

-   Blank workflow

    -   annotation of blank features

    -   removal of blank features by different computational approaches
        (optional)

-   Quantification workflow

    -   automated, interactive assignment of quantification levels

    -   annotation of all features with the nearest neighbor internal
        standard or a preselected internal standard, if defined in the
        `substance_table`

    -   normalization of all features to the assigned internal standard

    -   modelling and trimming of the quantification models

    -   fitting of quantification models

    -   interactive trimming of compounds with failed automated trimming

    -   automated quantification of the samples and export to a `csv`
        file

## Implementation details

-   `MZquant` is implemented as an [R](https://www.r-project.org/)
    package and optimized for R versions `>=4.2.1`.

-   `MZquant` uses `tibble` as the table format and builds on
    `tidyverse` functions for a performing processing in the main part
    of the code.

-   The `MZquant` processing script provides the full workflow and a
    `yaml` file containing all necessary settings for the processing.

-   The latter implementation ensures a reproducible environment of
    processing.

-   On success, all processing steps trigger the storage of the project
    stage in a `Rdata` file for easy re-run of single steps without need
    of full reprocessing.

-   The workflow requires user interaction at 3 times, where a review
    and edit of the intermediate results is necessary.

-   `MZquant` provides a full demonstration project and a comprehensive
    vignette.

-   A `continuous integration (CI)` pipeline utilizes the containers
    `bioconductor_docker:latest`, respectivelly, the `bioconductor_docker:devel`
    in the `devel` branch and the `bioconductor_docker:latest` container in the
    `main` branch to implement known best practices in package building.

-   Tagged versions are automatically deployed as releases using a `CI/CD`
    pipeline utilizing the `bioconductor_docker:latest` building of the
    package `tarball`, and deployment of the release including the
    `MZquant_{tag}.tar.gz` and the `MZquant_{tag}.build.log` as artifacts.
    The `tarball` contains the pre-build vignettes and enables users not
    experienced in package building to install a full version of `MZquant`.

-   Additional regularly `styler` and `BiocCheck` runs ensure code readability
    and the application of additional best practices.

## Installation

-   `MZquant` can be installed by:

    -   Downloading the lastest
        [`MZquant_0.8.3.tar.gz`](https://git.ufz.de/api/v4/projects/3744/packages/generic/MZquant/0.8.3/MZquant_0.8.3.tar.gz)
        and manual installation (best practice for most users)

    -   Installation directly from the repository (for experienced
        users):

            remotes::install_gitlab(
                repo = "wana_public/mzquant",
                host = "https://git.ufz.de",
                dependencies = TRUE,
                upgrade = "ask",
                build = TRUE,
                build_manual = TRUE,
                build_vignettes = TRUE
            )

    -   Note: The `tarball` contains the ready compiled manual and
        vignettes, while the latter method requires compilation.

## Getting started

For a quick start run:

    # Load the package
    library(MZquant)

    # Path of the project folder, add the full path to your
    # preferred empty working directory

    project_folder <- "path_to_your_workingdirectory"

    # Initialization and setup of the project folder.
    # default_settings = TRUE loads the default settings file
    # default_example = TRUE loads the example files

    initialise_project(project_folder, settings_file,
        default_processing_script = TRUE,
        default_settings = TRUE,
        default_example = TRUE
    )

    # Browse the vignettes for detailled information
    # and hints for the processing:

    browseVignettes("MZquant")

## Future plans

-   Integration of a logging tool to ensure full provenance of the
    processing.

-   Integration of a `Shiny` app for visual aided manual trimming.

-   Regristration of the package in `Bioconductor`

## Contributions

Your comments, recommendations, and hints on bugs are appreciated.

Use the [`issue tracker`](https://git.ufz.de/wana_public/mzquant/-/issues) or
send an email to [Tobias Schulze](mailto:tobias.schulze\@ufz.de).

The `issue tracker` requires an `GitHub` or `UFZ` login.
