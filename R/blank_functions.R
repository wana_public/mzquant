#' @title Blank feature tagging
#' @description
#' This internal function tags the features which are above the blank
#' feature threshold.
#'
#' @param object A tibble containing the feature data and
#' the \code{blank_feature_threshold}
#'
#' @author Tobias Schulze
#'
#' @seealso \code{\link{blank_threshold_default}}
#' @seealso \code{\link{blank_threshold_qt}}
#' @seealso \code{\link{blank_workflow}}
#'

tag_blanks <- function(object = object) {
    length_progressbar <- ncol(object) - 3
    pb <- progress::progress_bar$new(
        format = " [MZquant] Blank tagging             [:bar] :percent ETA: :eta",
        total = length_progressbar, clear = FALSE, width = 80
    )
    object <- data.table::data.table(object)
    for (i in 4:ncol(object)) {
        for (j in seq_len(length.out = nrow(object))) {
            if (is.na(dplyr::pull(object[j, ..i]))) {
                # No value in field
                next
            } else if (is.na(object$blank_features_threshold[j])) {
                # Threshold is na
                next
            } else if (dplyr::pull(object[j, ..i]) > object$blank_features_threshold[j]) {
                # not really necessary, but just for curiosity
                next
            } else if (dplyr::pull(object[j, ..i]) <= object$blank_features_threshold[j]) {
                # Value is below threshold
                if (dplyr::pull(object[j, ..i]) > 0) {
                    object[j, i] <- dplyr::pull(object[j, ..i]) * -1
                }
            }
        }

        pb$tick()
    }
    pb$terminate()

    return(dplyr::as_tibble(object))
}
#' @title Student t distribution based blank threshold estimation
#' @description
#' This internal function calculates the Student t distribution based
#' blank threshold for each feature based on the method detection limit (MDL)
#' estimation method of US EPA \insertCite{RN8520}{MZquant}.
#'
#' The method uses a distribution controlled factor for the addition of
#' the standard deviation (sd) to the mean value:
#'
#' Case 1: n >= minimum number of valid blank values
#'
#' \eqn{blank threshold = mean + qt(p, df = n - 1) * sd}
#'
#' Where \eqn{mean} is the average of the blank feature values, \eqn{qt}
#' is the \code{Students t} density function, \eqn{p} is the probability,
#' \eqn{df} are the degrees of freedom, \eqn{n} is the number of blank values
#' and \eqn{sd} is the standard deviation.
#'
#' Case 2: n < minimum number of valid blank values
#'
#' \eqn{blankthreshold = mean + qt(p, df = 1) * sd}
#'
#' The parameters \eqn{p} (\code{p = alpha}) and \eqn{n}
#' (n = \code{blank_qt_threshold}) are set in the \code{settings.yaml}.
#'
#' \code{blank_gt_alpha} is the probability of the Students t density
#' function (default: 0.99).
#' \code{blank_qt_threshold} is the minimum number of valid blank values
#' for the calculation of degrees of freeedom (see \code{\link[stats]{TDist}}).
#' \code{blank_qt_tail} is the lower tail is considered
#' in the \code{[Stats]{qt}} function (default: TRUE) (not used yet).
#'
#' @param object A list containing the number of valid blank
#' features \code{blank_features_detected}, the blank features
#' mean \code{blank_features_mean} and the blank features standard
#' deviation \code{blank_features_sd}.
#'
#' @param alpha alpha or probability p for the qt estimation (default: 0.99)
#'
#' @param threshold Minimum number of valid blank features for the
#' use the qt estimation (default: 3)
#'
#' @author Tobias Schulze
#'
#' @seealso \code{\link[stats]{TDist}}
#' @seealso \code{\link{blank_threshold_default}}
#' @seealso \code{\link{blank_workflow}}
#' @seealso \code{\link{tag_blanks}}
#'
#' @references
#' \insertRef{RN8520}{MZquant}
#'

blank_threshold_qt <- function(object = object, alpha = NA, threshold = NA) {
    blank_features_detected <- object[1]
    blank_features_mean <- object[2]
    blank_features_sd <- object[3]


    if (blank_features_detected == 0 || is.na(blank_features_detected)) {
        blank_features_threshold <- NA
    } else if (blank_features_detected <
        threshold & blank_features_detected > 0) {
        blank_features_threshold <-
            blank_features_mean + stats::qt(
                p = alpha, df = 1, lower.tail = TRUE
            ) * blank_features_sd
    } else {
        blank_features_threshold <-
            blank_features_mean + stats::qt(
                p = alpha, df = blank_features_detected - 1
            ) * blank_features_sd
    }
}

#' @title Default blank feature threshold estimation
#' @description
#' This internal function calculates a simple blank threshold based
#' on the mean and the standard deviation:
#'
#' \eqn{blankthreshold = mean + blankfactor * sd}
#'
#' Where \eqn{mean} is the average of the valid blank values,
#' the \eqn{blankfactor} is a fixed factor to control the
#' magnitude of the standard deviation \eqn{sd}.
#'
#' The parameter \code{blank_factor} is set in the section
#' \code{processing} in the \code{MZquant_settings.yaml}.
#'
#' @param object A list containing the number of valid blank
#' features \code{blank_features_detected}, the blank features
#' mean \code{blank_features_mean} and the blank features standard
#' deviation \code{blank_features_sd}.
#'
#' @param blank_factor Multiplier of the standard deviation for the
#' default blank correction method
#'
#' @author Tobias Schulze
#'
#' @seealso \code{\link{blank_threshold_qt}}
#' @seealso \code{\link{blank_workflow}}
#' @seealso \code{\link{tag_blanks}}
#'

blank_threshold_default <- function(object = object, blank_factor = NA) {
    blank_features_detected <- object[1]
    blank_features_mean <- object[2]
    blank_features_sd <- object[3]


    if (blank_features_detected == 0 || is.na(blank_features_detected)) {
        blank_features_threshold <- NA
    } else {
        blank_features_threshold <-
            blank_features_mean + blank_factor * blank_features_sd
    }
}
